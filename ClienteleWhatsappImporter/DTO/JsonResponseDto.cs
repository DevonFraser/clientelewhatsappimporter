﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClienteleWhatsappImporter.DTO
{
    public class JsonResponseDto
    {
        public bool Success { get; set; }
        public string Payload { get; set; }
    }
}
