﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClienteleWhatsappImporter.DTO
{
    public class MessageDto
    {
        public string Title { get; set; }
        public string Body { get; set; }
        public int MessageType { get; set; }
        public string DeviceNumber { get; set; }
    }
}
