﻿using ClienteleWhatsappImporter.DTO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ClienteleWhatsappImporter.Integration
{
    public class API
    {
        private static string _apiBase = "http://whatsappleads.mashlab-qa.co.za/api/Leads/GenerateLead";       
        private static WebClient _client = new WebClient();
        private string _address = "";
        private string _clientResult = "";      

        public API()
        {

        }

        public JsonResponseDto PostLead(MessageDto payload)
        {
            JsonResponseDto result = new JsonResponseDto();
            _client.BaseAddress = _apiBase;

            try
            {
                _client.Headers.Add("Content-Type", "application/json");
                _clientResult = _client.UploadString(_address, "POST", JsonConvert.SerializeObject(payload));

                if (_clientResult != null)
                {
                    //READ WHAT HAPPEND
                    result = JsonConvert.DeserializeObject<JsonResponseDto>(_clientResult);
                }
            }
            catch (WebException we)
            {
                result.Success = false;
                result.Payload = we.ToString();
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Payload = ex.Message;
            }

            return result;
        }
    }
}
