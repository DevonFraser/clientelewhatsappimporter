﻿using ClienteleWhatsappImporter.DTO;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClienteleWhatsappImporter.Integration;
using System.Diagnostics;

namespace ClienteleWhatsappImporter
{
    public class Program
    {
        static void Main(string[] args)
        {
            Trace.Listeners.Clear();
            TextWriterTraceListener twtl = new TextWriterTraceListener(Path.Combine(@"C:\Repositories\ClienteleWhatsappImporter\Logs", string.Format("ImportTrace_{0}.txt", DateTime.Now.ToString("yyyy-MM-dd-HHmmss"))));
            twtl.Name = "TextLogger";
            twtl.TraceOutputOptions = TraceOptions.ThreadId | TraceOptions.DateTime;

            ConsoleTraceListener ctl = new ConsoleTraceListener(false);
            ctl.TraceOutputOptions = TraceOptions.DateTime;

            Trace.Listeners.Add(twtl);
            Trace.Listeners.Add(ctl);
            Trace.AutoFlush = true;

            string fullFileName = @"C:\Repositories\ClienteleWhatsappImporter\ClienteleWhatsappImporter\ClienteleWhatsappImporter\29Oct10940(2).xlsx";

            //CHECK IF HAS VALID SPREAD SHEET
            ExcelPackage package = null;
            ExcelWorksheet sheet = null;

            using (Stream reader = File.OpenRead(fullFileName))
            {
                try
                {
                    package = new ExcelPackage(reader);
                    sheet = package.Workbook.Worksheets[1];
                }
                catch (Exception ex)
                {
                    Console.WriteLine(string.Format("Error: {0}", ex.Message));                    
                }

                if (sheet != null)
                {                   
                    int successCount = 0;
                    int failedCount = 0;

                    int rows = sheet.Dimension.End.Row;
                    int columns = sheet.Dimension.End.Column;

                    API api = new API();
                    //CHECK IF THERE ARE ROWS AND COLUMNS
                    if (rows > 0 && columns > 0)
                    {                       
                        for (int ri = 1; ri <= rows; ri++)
                        {                            
                            JsonResponseDto result = new JsonResponseDto();

                            string phone = "";                            
                            phone = sheet.Cells[ri, 1].Value?.ToString() ?? "";

                            if (!string.IsNullOrEmpty(phone))
                            {
                                MessageDto data = new MessageDto()
                                {
                                    Body = "Manual Bulk Import",
                                    DeviceNumber = "0725555568", //Funeral 0711027610 //Help 0725555568 
                                    MessageType = 1, //11191 = 2 / 11190 = 3 / 11189 = 1 / 10942 = 2 / 10941 = 3 / 10940 = 1
                                    Title = phone
                                };

                                //CLIENT CONNECTION API CALL
                                result = api.PostLead(data);

                                Console.WriteLine(string.Format("Import result - Status: {0} Payload {1} for number {2}", result.Success.ToString(), result.Payload, phone));
                            }
                            else
                            {
                                result.Success = false;
                                result.Payload = "No Phone supplied";
                            }

                            if (result.Success)
                            {
                                successCount++;
                            }
                            else
                            {
                                failedCount++;
                            }
                        }

                        Console.WriteLine(string.Format("Succesfully import: {0}", successCount));
                        Console.WriteLine(string.Format("Failed to import: {0}", failedCount));
                    }
                    else
                    {
                        Console.WriteLine(string.Format("There is no data in the file"));
                    }
                }
                else
                {
                    Console.WriteLine(string.Format("Sheet contains no data"));
                }
            }

            Console.ReadKey();
        }
    }
}
